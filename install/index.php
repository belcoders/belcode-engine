<?php
session_start();

use \vendor\core\Template;
use \vendor\core\Files;

define("APP", "Belcode Engine Install");
define("TYPE", "install");
define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
define("METHOD", $_SERVER["REQUEST_METHOD"]);

require ROOT."/app/autoload.php";
require ROOT."/app/functions.php";

if(isset($_GET["clear"])){
	unset($_SESSION["step"]);
	redirect("?step=1");
}

if(!isset($_GET["step"]) || $_GET["step"] == "") redirect("?step=1");
$step = intval($_GET["step"]);

if(!isset($_SESSION["step"])) $_SESSION["step"] = 1;
$current_step = $_SESSION["step"];

switch($step){
	case 1:
		if($current_step != 1) redirect("?step=".$current_step);
		
		if(METHOD == "POST"){
			// Проверка на то, ввел ли пользователь нужные данные для подключения к базе данных
			if(empty($_POST["host"]) || empty($_POST["user"]) || empty($_POST["db_name"])){
				send_msg("install_step_1", "warning", "Вы заполнили не все поля.");
				redirect("?step=1");
			}

			// Запись данных в переменные
			if(isset($_POST["is_clear"])) $is_clear = TRUE;
			else $is_clear = FALSE;

			$host = $_POST["host"];
			$user = $_POST["user"];
			$password = $_POST["password"];
			$db_name = $_POST["db_name"];

			// Проверка подключения к базе данных
			$db_connect_check = mysqli_connect($host, $user, $password, $db_name);
			if(!$db_connect_check){
				send_msg("install_step_1", "danger", "Не удалось подключиться к базе данных.");
				redirect("?step=1");
			}

			// Очистка базы данных
			if($is_clear){
				$result = mysqli_query($db_connect_check, "SHOW TABLES");

				while($table = mysqli_fetch_array($result)){
					$result = mysqli_query($db_connect_check, "DROP TABLE ".$table[0]);
					if(!$result){
						send_msg("install_step_1", "danger", "Не удалось очистить базу данных.");
						redirect("?step=1");
					}
				}
			}

			// Создание таблиц движка
			$sql = "CREATE TABLE `users` ( `id` INT NOT NULL AUTO_INCREMENT , `login` VARCHAR(64) NOT NULL , `password` VARCHAR(32) NOT NULL , `email` VARCHAR(128) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
			$result = mysqli_query($db_connect_check, $sql);

			if(!$result){
                send_msg("install_step_1", "danger", "Не удалось создать таблицу с пользователями.");
                redirect("?step=1");
            }

			$sql = "CREATE TABLE `categories` ( `id` INT NOT NULL AUTO_INCREMENT , `link` VARCHAR(128) NOT NULL , `name` VARCHAR(128) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";

			$result = mysqli_query($db_connect_check, $sql);

            if(!$result){
                send_msg("install_step_1", "danger", "Не удалось создать таблицу с категориями.");
                redirect("?step=1");
            }

            // Закрытие соединения с базой данных
			mysqli_close($db_connect_check);

			// Создание конфигурации соединения к базе данных
			$db_data = [
				"host" => $host,
				"user" => $user,
				"password" => $password,
				"db_name" => $db_name
			];
            Files::configSet("db", $db_data);

			// Повышение шага установки и перенаправление на новый шаг
			$_SESSION["step"] = 2;
			redirect("?step=2");
		}
		else
		{
			// Отображение страницы
			Template::view("template/header", ["title" => "Установка"]);
            Template::view("step_1");
            Template::view("template/footer");
		}
	break;
	case 2:
		if($current_step != 2) redirect("?step=".$current_step);
		
		if(METHOD == "POST"){
            if(empty($_POST["site_name"])){
                send_msg("install_step_2", "warning", "Вы заполнили не все поля.");
                redirect("?step=2");
            }

            $site_name = $_POST["site_name"];

            $main_data = [
                "site_name" => $site_name,
				"theme" => "default"
            ];
            Files::configSet("main", $main_data);
            $_SESSION["step"] = 3;
            redirect("?step=3");
		}
		else
		{
            Template::view("template/header", ["title" => "Установка"]);
            Template::view("step_2");
            Template::view("template/footer");
		}
	break;
    case 3:
        if($current_step != 3) redirect("?step=".$current_step);

        if(METHOD == "POST"){
        	if(empty($_POST["login"]) || empty($_POST["password"]) || empty($_POST["password2"]) || empty($_POST["email"])){
                send_msg("install_step_3", "warning", "Вы заполнили не все поля.");
                redirect("?step=3");
            }

            $login = $_POST["login"];
            $password = $_POST["password"];
            $password2 = $_POST["password2"];
            $email = $_POST["email"];

            if($password != $password2){
            	send_msg("install_step_3", "danger", "Вы неправильно повторили пароль.");
                redirect("?step=3");
            }

            /*if(!User::validLogin($login) || !User::validPassword($password) || !User::validEmail($email)){
            	send_msg("install_step_3", "danger", "Вы ввели невалидные данные, перечитайте правила.");
                redirect("?step=3");
            }*/
            if(!preg_match("~^[a-zA-Z0-9_-]+$~", $login) || strlen($login) < 4 || strlen($login) > 64){
            	send_msg("install_step_3", "danger", "Вы ввели невалидный логин, перечитайте правила.");
                redirect("?step=3");
            }

            if(strlen($password) < 6 && strlen($password) > 64){
            	send_msg("install_step_3", "danger", "Вы ввели невалидный пароль, перечитайте правила.");
                redirect("?step=3");
            }

            if(strlen($email) < 4 && strlen($email) > 128){
            	send_msg("install_step_3", "danger", "Вы ввели невалидный логин, перечитайте правила.");
                redirect("?step=3");
            }

            $db_config = Files::configGet("db");
            $db = mysqli_connect($db_config["host"], $db_config["user"], $db_config["password"], $db_config["db_name"]);
            if(!$db){
				send_msg("install_step_3", "danger", "Не удалось подключиться к базе данных.");
				redirect("?step=3");
			}

			$result = mysqli_query($db, "INSERT INTO users(login,password,email) VALUES ('".$login."','".md5($password)."','".$email."')");
			if(!$result){
				send_msg("install_step_3", "danger", "Не удалось выполнить запрос.");
				redirect("?step=3");
			}
			$_SESSION["step"] = 4;
			redirect("?step=4");
        }
        else
        {
            Template::view("template/header", ["title" => "Установка"]);
            Template::view("step_3");
            Template::view("template/footer");
        }
    break;
    case 4:
    	if($current_step != 4) redirect("?step=".$current_step);

        Template::view("template/header", ["title" => "Установка завершена"]);
        Template::view("step_4");
        Template::view("template/footer");

        unset($_SESSION["step"]);
    break;
	default:
		redirect("?step=1");
	break;
}