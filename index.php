<?php
ini_set("session.gc_maxlifetime", 0);
session_start();

define("APP", "Belcode Engine");
define("TYPE", "global");
define("APP_VERSION", "0.1.2");
define("STAGE", "beta");
define("BUILD", "9");

define("ROOT", $_SERVER["DOCUMENT_ROOT"]);

define("URI", $_SERVER["REQUEST_URI"]);
define("METHOD", $_SERVER["REQUEST_METHOD"]);

if(isset($_GET["do"])) $do = $_GET["do"];
else $do = "index";

if(isset($_GET["action"])) $action = $_GET["action"];
else $action = "main";

define("CTRL", $do);
define("ACT", $action);

require ROOT."/app/autoload.php";
require ROOT."/app/functions.php";

if($do == "install") redirect("/install/");
if(!file_exists(ROOT."/config/db.php") || !file_exists(ROOT."/config/main.php")){
    if(!file_exists(ROOT."/install/index.php")) die("Не удалось выполнить установку: <b>файл <i>/install/index.php</i> не найден.</b>.");
    redirect("/install/");
}

$main_config = \vendor\core\Files::configGet("main");
$db_config = \vendor\core\Files::configGet("db");

define("SITE_NAME", $main_config["site_name"]);
define("THEME", $main_config["theme"]);
define("THEME_PATH", "/themes/".$main_config["theme"]);

define("DB_HOST", $db_config["host"]);
define("DB_USER", $db_config["user"]);
define("DB_PASSWORD", $db_config["password"]);
define("DB_NAME", $db_config["db_name"]);

$alias_str = $do."@".$action;
$aliases = include(ROOT."/app/aliases.php");
foreach($aliases as $key => $alias){
    if($alias_str == $key){
        $req_arr = explode("@", $alias);
        $do = $req_arr[0];
        $action = $req_arr[1];
        break;
    }
}

$controller = 'app\controllers\\'.ucfirst($do)."Controller";
$method = $action.ucfirst(strtolower(METHOD));


if(!method_exists($controller, $method)) notfound();

if(is_dir(ROOT."/install")) send_syst_msg("warning", "Папка <i>install</i> не удалена из корня сайта. Удалите эту папку для предотвращения повторной установки скрипта.");

$cObject = new $controller();
$cObject->$method();