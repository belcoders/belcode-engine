<?php
namespace vendor\core;

defined("APP") or die("Hacking attempt!");

class Files
{
    public static function configGet($file){
        $config = include(ROOT."/config/".$file.".php");
        return $config;
    }

    public static function configSet($file, $vars){
        if(!is_dir(ROOT."/config")) mkdir(ROOT."/config");
        if(!file_exists(ROOT."/config/".$file.".php")) touch(ROOT."/config/".$file.".php");

        $config = "<?php defined('APP') or die('Hacking attempt!');".PHP_EOL;
        $config .= "return [".PHP_EOL;
        foreach($vars as $key => $var){
            $config .= "'".$key."' => '".$var."',".PHP_EOL;
        }
        $config .= "];";

        file_put_contents(ROOT."/config/".$file.".php", $config);
    }
}