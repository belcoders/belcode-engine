<?php
namespace vendor\core;

defined("APP") or die("Hacking attempt!");

class Template
{
	public static function set_app(){
		$app = APP;
		
		switch($app){
			case "Belcode Engine":
				return ROOT."/themes/".THEME."/views";
			break;
			case "Belcode Engine Install":
				return ROOT."/install/theme/views";
			break;
		}
	}
	
	public static function view($view, $vars = []){
		$views_root = self::set_app();
		
		if(!empty($vars)){
			foreach($vars as $key => $var){
				$$key = $var;
			}
		}
		
		include($views_root."/".$view.".phtml");
	}

	public static function navActive($ctrl, $act = NULL){
	    if(!empty($act)){
	        if($ctrl == CTRL && $act == ACT) return ' class="active"';
        }
        else
        {
            if($ctrl == CTRL) return ' class="active"';
        }
    }
}