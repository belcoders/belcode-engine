<?php
namespace vendor\core;

defined("APP") or die("Hacking attempt!");

class BE_Controller
{
    public function __construct(){
        if(isset($_GET)){
            foreach($_GET as $key => $vars){
                if($key != "do" && $key != "action"){
                    $this->$key = $vars;
                }
            }
        }
    }
}