<?php
namespace app\controllers;

defined("APP") or die("Hacking attempt!");

use vendor\core\BE_Controller;
use vendor\core\Template;
use vendor\database\SafeMySQL;

class IndexController extends BE_Controller
{
    public function mainGet(){
    	$mysql = new SafeMySQL();
    	$categories = $mysql->getAll("SELECT * FROM categories");

        Template::view("template/header", ["title" => "Главная"]);
        Template::view("index", ['categories' => $categories, 'currentCategory' => (isset($this->category)) ? $this->category : NULL]);
        Template::view("template/footer");
    }

    public function loginPost(){

    }

    public function loginGet(){
        
    }

    public function registerGet(){

    }
}