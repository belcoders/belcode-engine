<?php defined("APP") or die("Hacking attempt!");

return [
    "login@main" => "index@login",
    "register@main" => "index@register",
];