<?php
defined("APP") or die("Hacking attempt!");

function debug($var){
	echo "<pre>".print_r($var)."</pre>";
}

function notfound(){
    echo "404";
    die();
}

function redirect($url){
    header("Location: ".$url);
    die();
}

function send_msg($name, $type, $text){
	$_SESSION["msg"][$name]["name"] = $name;
	$_SESSION["msg"][$name]["type"] = $type;
	$_SESSION["msg"][$name]["text"] = $text;
}

function show_msg($name){
	if(isset($_SESSION["msg"][$name])){
		$name = $_SESSION["msg"][$name]["name"];
		$type = $_SESSION["msg"][$name]["type"];
		$text = $_SESSION["msg"][$name]["text"];

		echo '<div class="container"><div class="alert alert-'.$type.'">'.PHP_EOL;
		echo $text.PHP_EOL;
		echo '</div></div>';

		unset($_SESSION["msg"][$name]);
	}
}

function send_syst_msg($type, $text){
	switch($type){
		case "danger":
			$head_text = "Ошибка!";
		break;
		case "warning":
			$head_text = "Внимание!";
		break;
		case "info":
			$head_text = "Информация:";
		break;
		case "success":
			$head_text = "Успех!";
		break;
	}

	$GLOBALS["syst_msg"][] = [
		"type" => $type,
		"text" => "<h4><b>".$head_text."</b></h4>".$text
	];
}

function show_syst_msg(){
	if(!empty($GLOBALS["syst_msg"])){
		foreach($GLOBALS["syst_msg"] as $msg){
			?>
            <div class="container"><div class="alert alert-<?=$msg['type']?>"><?=$msg['text']?></div></div>
			<?php
		}
	}
}

function genUrl($ctrl, $act = NULL, array $vars = []){
    $url = '';
    if($ctrl != 'index') $url = "/?do=".$ctrl;
    if(!empty($act) && $act != 'main') $url .= (!empty($url)) ? '&action='.$act : '?action='.$act;
    if(!empty($vars)){
        foreach($vars as $key => $var){
            $url .= (!empty($url)) ? '&'.$key.'='.$var : '?'.$key.'='.$var;
        }
    }
    return $url;
}