<?php
namespace app\models;

defined("APP") or die("Hacking attempt!");

class User
{
    public static function showLoginRules(){
        $msg = '<div class="alert alert-info">'.PHP_EOL;
        $msg .= '<h4><b>Правила регистрации:</b></h4>'.PHP_EOL;
        $msg .= '<b>Логин:</b> <i>символы <b>английского алфавита</b> люборого регистра, <b>-</b> (<b>дефис</b>), <b>_</b> (<b>нижнее подчеркивание</b>)<br>'.PHP_EOL.'количество символов - <b>от 4-х до 64-х</b> включая <b>4 и 64</b>;</i><br>'.PHP_EOL;
        $msg .= '<b>Почта:</b> <i>количество символов - <b>от 4-х до 128-и</b> включая <b>4 и 128</b>;</i><br>'.PHP_EOL;
        $msg .= '<b>Пароль:</b> <i>количество символов - <b>от 6-и до 64-х</b> включая <b>6 и 64</b>.</i><br>'.PHP_EOL;
        $msg .= '</div>';
        return $msg;
    }
    public static function validLogin($login){
        if(preg_match("~^[a-zA-Z0-9_-]+$~", $login) && strlen($login) >= 4 && strlen($login) <= 64) return true;
        else return false;
    }
    public static function validPassword($password){
        if(strlen($password) >= 6 && strlen($password) <= 64) return true;
        else return false;
    }
    public static function validEmail($email){
        if(strlen($email) >= 4 && strlen($email) <= 128) return true;
        else return false;
    }
}